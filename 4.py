def fib(n):
    starting_no = 0
    oldest_number = 0
    older_number = 1
    current_number = 0
    if n > 0:
      n = n+1
      
    for i in range(n):
        if i == 0:
            current_number = oldest_number
        elif i == 1:
            current_number = older_number
        else:
            current_number = older_number + oldest_number
            oldest_number = older_number
            older_number = current_number
    return current_number


temp = input('Enter the nth number of fibonacci sequesnce to be determined')
temp = int(temp)
print(fib(temp))